from agile_system.board import Board

import unittest

from agile_system.task import Task
from agile_system.user import User


class TestBoard(unittest.TestCase):
    def test_board_initialization(self):
        b1_name = "board1"
        b1: Board = Board(b1_name)

        self.assertEqual(b1._name, b1_name)
        self.assertEqual(b1._user_ids, set())
        self.assertEqual(b1._task_ids, set())

        b2_name = "board2"
        b2_user_ids = {1, 2, 3}
        b2_task_ids = {4, 5, 6}
        b2_unique_id = 42
        b2: Board = Board(b2_name, user_ids=b2_user_ids, task_ids=b2_task_ids, unique_id=b2_unique_id)

        self.assertEqual(b2._name, b2_name)
        self.assertEqual(b2._user_ids, b2_user_ids)
        self.assertEqual(b2._task_ids, b2_task_ids)
        self.assertEqual(b2._unique_id, b2_unique_id)

    def test_board_getters(self):
        b_name = "board"
        b_user_ids = {1, 2, 13}
        b_task_ids = {4, 42, 6}
        b_unique_id = 42

        b: Board = Board(b_name, user_ids=b_user_ids, task_ids=b_task_ids, unique_id=b_unique_id)

        self.assertEqual(b.get_name(), b_name)
        self.assertEqual(b.get_user_ids(), b_user_ids)
        self.assertEqual(b.get_task_ids(), b_task_ids)
        self.assertEqual(b.get_unique_id(), b_unique_id)

    def test_board_setters(self):
        b: Board = Board("temp")
        b_new_name = "new board"
        b.set_name(b_new_name)

        self.assertEqual(b.get_name(), b_new_name)

    def test_board_add_user(self):
        b: Board = Board("board")
        b_new_user_id = 42
        u: User = User("user", unique_id=b_new_user_id)
        b.add_user(u)

        self.assertEqual(b.get_user_ids(), {b_new_user_id})

    def test_board_remove_user(self):
        user_id = 42
        b: Board = Board("board", user_ids={user_id})
        u: User = User("user", unique_id=user_id)
        b.remove_user(u)

        self.assertEqual(b.get_user_ids(), set())

    def test_board_add_task(self):
        b: Board = Board("board")
        new_task_id = 42
        t: Task = Task("task", "desc", unique_id=new_task_id)
        b.add_task(t)

        self.assertEqual(b.get_task_ids(), {new_task_id})

    def test_board_remove_task(self):
        task_id = 42
        b: Board = Board("board", task_ids={task_id})
        t: Task = Task("task", "desc", unique_id=task_id)
        b.remove_task(t)

        self.assertEqual(b.get_task_ids(), set())

    def test_board_equality(self):
        b1: Board = Board("board1")
        b2: Board = Board("board1", unique_id=b1.get_unique_id())
        self.assertEqual(b1, b2)

        user_ids = {9, 10, 11}
        task_ids = {101, 100, 103}
        b3: Board = Board("board3", user_ids=user_ids, task_ids=task_ids)
        b4: Board = Board("board3", user_ids=user_ids, task_ids=task_ids, unique_id=b3.get_unique_id())

        self.assertEqual(b3, b4)

        b5 = b3

        self.assertEqual(b5, b3)
