import unittest

from agile_system.board import Board
from agile_system.organization import Organization
from agile_system.task import Task
from agile_system.user import User


class TestOrganization(unittest.TestCase):
    org: Organization = None

    def add_test_data(self):
        t1: Task = Task("task1", "desc1", unique_id=0)
        t2: Task = Task("task2", "desc2", unique_id=1)
        self.org.add_task(t1)
        self.org.add_task(t2)

        u1: User = User("user1", unique_id=0)
        u2: User = User("user2", unique_id=1)
        self.org.add_user(u1)
        self.org.add_user(u2)

        b1: Board = Board("board1", unique_id=0)
        b2: Board = Board("board2", unique_id=1)
        self.org.add_board(b1)
        self.org.add_board(b2)

    @classmethod
    def setUpClass(cls) -> None:
        cls.org = Organization.get_instance()

    def setUp(self) -> None:
        self.org.clear_organization()
        self.add_test_data()

    def test_assign_task_to_user(self):
        task_id = 0
        user_id = 0
        task = self.org.get_task_from_id(task_id)
        user = self.org.get_user_from_id(user_id)

        self.org.assign_task_to_user(task, user)

        # check that task has been added to user and that user task assignment has been created
        self.assertEqual(self.org.get_user_from_id(user_id).get_task_ids(), {task_id})
        self.assertEqual(self.org.get_user_task_assignment(task), self.org.get_user_from_id(user_id))

    def test_assign_task_to_user_incorrect(self):
        task_id = 0
        user_id = 0
        task = self.org.get_task_from_id(task_id)
        user = self.org.get_user_from_id(user_id)

        self.org.assign_task_to_user_incorrect(task, user)

        # check that task has been added to user and that user task assignment has been created
        self.assertEqual(self.org.get_user_from_id(user_id).get_task_ids(), {task_id})
        self.assertEqual(self.org.get_user_task_assignment(task), self.org.get_user_from_id(user_id))  # failure

    def test_remove_task_from_user(self):
        task_id = 0
        user_id = 0
        task = self.org.get_task_from_id(task_id)
        user = self.org.get_user_from_id(user_id)

        self.org.assign_task_to_user(task, user)

        user = self.org.get_user_from_id(user_id)

        self.org.remove_user_task_assignment(task, user)

        # check that task has been removed from user
        self.assertEqual(self.org.get_user_from_id(user_id).get_task_ids(), set())

    def test_change_task_user_assignment(self):
        task_id = 0
        user_id = 0
        user2_id = 1
        task = self.org.get_task_from_id(task_id)
        user = self.org.get_user_from_id(user_id)
        user2 = self.org.get_user_from_id(user2_id)

        self.org.assign_task_to_user(task, user)
        self.org.change_task_user_assignment(task, user2)

        # check that task has been removed from user1 and added to user2
        self.assertEqual(self.org.get_user_from_id(user_id).get_task_ids(), set())
        self.assertEqual(self.org.get_user_from_id(user2_id).get_task_ids(), {task_id})
        self.assertEqual(self.org.get_user_task_assignment(task), self.org.get_user_from_id(user2_id))

    def test_assign_task_to_board(self):
        task_id = 0
        board_id = 0
        task = self.org.get_task_from_id(task_id)
        board = self.org.get_board_from_id(board_id)

        self.org.assign_task_to_board(task, board)

        # check that task has been added to board and that board task assignment has been created
        self.assertEqual(self.org.get_board_from_id(board_id).get_task_ids(), {task_id})
        self.assertEqual(self.org.get_board_task_assignment(task), self.org.get_board_from_id(board_id))

    def test_remove_task_from_board(self):
        task_id = 0
        board_id = 0
        task = self.org.get_task_from_id(task_id)
        board = self.org.get_board_from_id(board_id)

        self.org.assign_task_to_board(task, board)

        board = self.org.get_board_from_id(board_id)

        self.org.remove_board_task_assignment(task, board)

        # check that board no longer has task assigned to it
        self.assertEqual(self.org.get_board_from_id(board_id).get_task_ids(), set())

    def test_change_task_board_assignment(self):
        task_id = 0
        board_id = 0
        board2_id = 1
        task = self.org.get_task_from_id(task_id)
        board = self.org.get_board_from_id(board_id)
        board2 = self.org.get_board_from_id(board2_id)

        self.org.assign_task_to_board(task, board)
        self.org.change_task_board_assignment(task, board2)

        # check that task has been removed from board1 and added to board2
        # and that the board task assignment has been updated
        self.assertEqual(self.org.get_board_from_id(board_id).get_task_ids(), set())
        self.assertEqual(self.org.get_board_from_id(board2_id).get_task_ids(), {task_id})
        self.assertEqual(self.org.get_board_task_assignment(task), self.org.get_board_from_id(board2_id))

    def test_assign_user_to_board(self):
        user_id = 0
        board_id = 0
        user = self.org.get_user_from_id(user_id)
        board = self.org.get_board_from_id(board_id)

        self.org.assign_user_to_board(user, board)

        # check that user has been added to board and that the user board assignment has been created
        self.assertEqual(self.org.get_board_from_id(board_id).get_user_ids(), {user_id})
        self.assertEqual(self.org.get_board_user_assignment(user), {self.org.get_board_from_id(board_id)})

        board2_id = 1
        board2 = self.org.get_board_from_id(board2_id)

        self.org.assign_user_to_board(user, board2)

        # test assigning user to multiple boards
        self.assertEqual(self.org.get_board_from_id(board2_id).get_user_ids(), {user_id})
        self.assertEqual(
            self.org.get_board_user_assignment(user),
            {self.org.get_board_from_id(board_id), self.org.get_board_from_id(board2_id)}
        )

    def test_remove_user_from_board(self):
        user_id = 0
        board_id = 0
        user = self.org.get_user_from_id(user_id)
        board = self.org.get_board_from_id(board_id)

        self.org.assign_user_to_board(user, board)

        board = self.org.get_board_from_id(board_id)

        self.org.remove_board_user_assignment(user, board)

        # remove user from one board
        self.assertEqual(self.org.get_board_from_id(board_id).get_user_ids(), set())

        board = self.org.get_board_from_id(board_id)

        board2_id = 1
        board2 = self.org.get_board_from_id(board2_id)

        self.org.assign_user_to_board(user, board2)
        self.org.assign_user_to_board(user, board)

        board = self.org.get_board_from_id(board_id)

        self.org.remove_board_user_assignment(user, board)

        # test that user has only been removed from board1
        self.assertEqual(self.org.get_board_from_id(board2_id).get_user_ids(), {user_id})

