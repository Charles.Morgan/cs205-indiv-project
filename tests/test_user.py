from typing import Set

from agile_system.task import Task
from agile_system.user import User

import unittest


class TestUser(unittest.TestCase):
    def test_user_initialization(self):
        u_name = "user"
        u: User = User(u_name)

        # test user values equal values used in initialization
        self.assertEqual(u._name, u_name)

    def test_user_ids(self):
        u: User = User("user")
        instance_offset = u.get_unique_id()
        for i in range(instance_offset, instance_offset + 101):
            u = User("user", unique_id=i)
            self.assertEqual(u._unique_id, i)

        custom_id = 1234
        u = User("user", unique_id=custom_id)
        self.assertEqual(u._unique_id, custom_id)

    def test_user_getters(self):
        u_name = "user"
        u_id = 42
        u_task_ids: Set[int] = {1, 2, 3}
        u: User = User(u_name, unique_id=u_id, task_ids=u_task_ids)

        # test values returned from getters equal values used to create object
        self.assertEqual(u.get_name(), u_name)
        self.assertEqual(u.get_unique_id(), u_id)
        self.assertEqual(u.get_task_ids(), u_task_ids)

    def test_user_setters(self):
        u: User = User("user")
        u_new_name = "new user"
        u.set_name(u_new_name)
        self.assertEqual(u.get_name(), u_new_name)

    def test_user_add_task(self):
        u1: User = User("user1")
        u2_task_ids: Set[int] = {1, 2, 3}
        u2: User = User("user2", task_ids=u2_task_ids)
        addtl_task_id = 42
        addtl_task = Task("task", "desc", unique_id=addtl_task_id)

        # assert task ids after adding task are equal to expected task ids
        u1.add_task(addtl_task)
        self.assertEqual(u1.get_task_ids(), {addtl_task_id})

        # adding id to set that already contains ids
        u2.add_task(addtl_task)
        u2_task_ids.add(addtl_task_id)
        self.assertEqual(u2.get_task_ids(), u2_task_ids)

    def test_user_remove_task(self):
        u1_task_ids = {42}
        u1: User = User("user1", task_ids=u1_task_ids)
        u2_task_ids: Set[int] = {1, 2, 3, 42}
        u2: User = User("user2", task_ids=u2_task_ids)
        task_id = 42
        task = Task("task", "desc", unique_id=task_id)

        # assert task ids after removing are empty
        u1.remove_task(task)
        u1_task_ids.remove(task_id)
        self.assertEqual(u1.get_task_ids(), u1_task_ids)

        # removing task from ids with ids still remaining
        u2.remove_task(task)
        u2_task_ids.remove(task_id)
        self.assertEqual(u2.get_task_ids(), u2_task_ids)

    def test_user_equality(self):
        u1: User = User("user1")
        u2: User = User("user1", unique_id=u1.get_unique_id())
        self.assertEqual(u1, u2)

        u_task_ids = {1, 2, 3}
        u3: User = User("user3", task_ids=u_task_ids)
        u4: User = User("user3", task_ids=u_task_ids, unique_id=u3.get_unique_id())
        self.assertEqual(u3, u4)

        t: Task = Task("task", "desc")
        u4.add_task(t)
        self.assertNotEqual(u3, u4)

        u5 = u3
        self.assertEqual(u5, u3)
