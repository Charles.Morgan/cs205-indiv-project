import unittest

from agile_system.task import Task


class TestTask(unittest.TestCase):
    def test_task_initialization(self):
        # create task
        t_name = "task"
        t_desc = "desc"
        t: Task = Task(t_name, t_desc)

        # assert task variables are equal to ones used to instantiate it
        self.assertEqual(t._name, t_name, "task name not correct")
        self.assertEqual(t._description, t_desc, "task desc not correct")

    def test_task_ids(self):
        t: Task = Task("task", "desc")

        # get instance offset and loop through 100 ids
        instance_offset = t.get_unique_id() + 1
        for i in range(instance_offset, instance_offset + 101):
            t = Task("task", "desc")
            # test that the unique id is correct
            self.assertEqual(t._unique_id, i)

        # make sure initialization is using the optional id if specified
        custom_id = 5678
        t = Task("task", "desc", unique_id=custom_id)
        self.assertEqual(t._unique_id, custom_id)

    def test_task_getters(self):
        t_name = "task"
        t_desc = "desc"
        t_id = 0
        t: Task = Task(t_name, t_desc, unique_id=t_id)

        # test getter values
        self.assertEqual(t.get_name(), t_name)
        self.assertEqual(t.get_description(), t_desc)
        self.assertEqual(t.get_unique_id(), t_id)
        self.assertFalse(t.is_completed())

    def test_task_setters(self):
        t: Task = Task("task", "desc")

        # test name setter
        t_new_name = "new task"
        t.set_name(t_new_name)
        self.assertEqual(t.get_name(), t_new_name)

        # test description setter
        t_new_desc = "new desc"
        t.set_description(t_new_desc)
        self.assertEqual(t.get_description(), t_new_desc)

        # test task completed setter
        t.set_completed(True)
        self.assertTrue(t.is_completed())

    def test_task_equals(self):
        t1: Task = Task("task1", "desc1")
        t2: Task = Task("task2", "desc2")
        t3: Task = Task("task1", "desc1")
        t4: Task = Task("task1", "desc1", unique_id=t1.get_unique_id())

        # test task equality
        self.assertNotEqual(t1, t2)
        self.assertNotEqual(t1, t3)
        self.assertEqual(t1, t4)
        self.assertEqual(t1, t1)
        t4.set_completed(True)
        self.assertNotEqual(t1, t4)

        t5 = t3
        self.assertEqual(t5, t3)
