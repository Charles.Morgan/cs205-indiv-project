import copy
from itertools import count
from typing import Set

from agile_system.task import Task
from agile_system.user import User


class Board:
    __ids = count(0)
    _unique_id: int
    _name: str
    _user_ids: Set[int]
    _task_ids: Set[int]

    def __init__(self, name: str, unique_id: int = None, user_ids: Set[int] = None, task_ids: Set[int] = None):
        if unique_id is None:
            self._unique_id = next(self.__ids)
        else:
            self._unique_id = unique_id
        self._name = name
        if user_ids is None:
            self._user_ids = set()
        else:
            self._user_ids = copy.deepcopy(user_ids)
        if task_ids is None:
            self._task_ids = set()
        else:
            self._task_ids = copy.deepcopy(task_ids)

    def get_unique_id(self) -> int:
        return self._unique_id

    def get_name(self) -> str:
        return self._name

    def get_user_ids(self) -> Set[int]:
        return copy.deepcopy(self._user_ids)

    def get_task_ids(self) -> Set[int]:
        return copy.deepcopy(self._task_ids)

    def set_name(self, new_name: str):
        self._name = new_name

    def add_user(self, u: User):
        """
        add user id to _user_ids set
        :param u: the user whose id will be added to _user_ids
        """
        if u is not None:
            self._user_ids.add(u.get_unique_id())

    def remove_user(self, u: User):
        """
        remove user id from _user_ids set
        :param u: the user whose id will be removed from _user_ids
        """
        if u is not None:
            self._user_ids.remove(u.get_unique_id())

    def add_task(self, t: Task):
        """
        add task id to _task_ids set
        :param t: the task whose id will be added to _task_ids
        """
        if t is not None:
            self._task_ids.add(t.get_unique_id())

    def remove_task(self, t: Task):
        """
        remove task id from _task_ids
        :param t: the task whose id will be removed from _task_ids
        """
        if t is not None:
            self._task_ids.remove(t.get_unique_id())

    def __str__(self):
        return f"""Board {str(self._unique_id)}
  Name: {str(self._name)}
  Task IDs: {str(self._task_ids)}
  User IDs: {str(self._user_ids)}"""

    def __repr__(self):
        return f"Board(unique_id: {repr(self._unique_id)}, " \
               f"name: {repr(self._name)}, " \
               f"users: {repr(self._user_ids)}, " \
               f"tasks: {repr(self._task_ids)})"

    def __eq__(self, other) -> bool:
        if other is None:
            return False

        if isinstance(other, Board):
            return self._unique_id == other._unique_id \
                   and self._name == other._name \
                   and self._user_ids == other._user_ids \
                   and self._task_ids == other._task_ids
        return False

    def __hash__(self):
        return hash((self._unique_id, self._name))


if __name__ == "__main__":
    pass
