import copy
from itertools import count
from typing import Set

from agile_system.task import Task


class User:
    __ids = count(0)
    _unique_id: int
    _name: str
    _task_ids: Set[int]

    def __init__(self, name: str, unique_id: int = None, task_ids: Set[int] = None):
        if unique_id is None:
            self._unique_id = next(self.__ids)
        else:
            self._unique_id = unique_id
        self._name = name
        if task_ids is None:
            self._task_ids = set()
        else:
            self._task_ids = copy.deepcopy(task_ids)

    def get_unique_id(self) -> int:
        return self._unique_id

    def get_name(self) -> str:
        return self._name

    def get_task_ids(self) -> Set[int]:
        """
        :return: a copy of the _task_ids set
        """
        return copy.deepcopy(self._task_ids)

    def set_name(self, name: str):
        self._name = name

    def clear_tasks(self):
        """
        removes all tasks from the users _task_id set
        """
        self._task_ids = set()

    def add_task(self, t: Task):
        """
        adds the specified task to the users _task_id list
        :param t: the task to add to the users _task_id list
        """
        if t is not None:
            self._task_ids.add(t.get_unique_id())

    def remove_task(self, t: Task):
        """
        removes the specified task from the users _task_id set
        :param t: the task to remove from the users _task_id set
        :return:
        """
        if t is not None:
            self._task_ids.remove(t.get_unique_id())

    def __str__(self):
        return f"""User {str(self._unique_id)}
  Name: {str(self._name)}
  Task IDs: {str(self._task_ids)}"""

    def __repr__(self):
        return f"User(unique_id: {repr(self._unique_id)}, " \
               f"name: {repr(self._name)}, " \
               f"task_ids: {repr(self._task_ids)})"

    def __eq__(self, other):
        if other is None:
            return False

        if isinstance(other, User):
            return self._unique_id == other._unique_id \
                   and self._name == other._name \
                   and self._task_ids == other._task_ids
        return False

    def __hash__(self):
        return hash((self._unique_id, self._name))


if __name__ == "__main__":
    pass
