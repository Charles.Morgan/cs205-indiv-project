import copy
from typing import Set, List, Dict

from agile_system.board import Board
from agile_system.task import Task
from agile_system.user import User


class Organization:
    __instance = None
    _boards: Set[Board]
    _users: Set[User]
    _tasks: Set[Task]
    # user id to set of task ids, tasks can only have one user assignment
    _user_task_assignments: Dict[int, Set[int]]
    # board id to set of task ids, tasks can only have one board assignment
    _board_task_assignments: Dict[int, Set[int]]
    # board id to set of user ids, users can have multiple board assignment
    _board_user_assignments: Dict[int, Set[int]]

    @classmethod
    def get_instance(cls):
        if cls.__instance is None:
            cls.__instance = Organization()
        return cls.__instance

    def __init__(self):
        self._boards = set()
        self._users = set()
        self._tasks = set()
        self._user_task_assignments = {}
        self._board_task_assignments = {}
        self._board_user_assignments = {}

    def clear_organization(self):
        self._boards = set()
        self._users = set()
        self._tasks = set()
        self._user_task_assignments = dict()
        self._board_task_assignments = dict()
        self._board_user_assignments = dict()

    def get_board_from_id(self, unique_id: int) -> Board:
        for board in self._boards:
            if board.get_unique_id() == unique_id:
                return copy.deepcopy(board)
        raise Exception("get_board_from_id() board not found", unique_id)

    def get_user_from_id(self, unique_id: int) -> User:
        for user in self._users:
            if user.get_unique_id() == unique_id:
                return copy.deepcopy(user)
        raise Exception("get_user_from_id() user not found", unique_id)

    def get_task_from_id(self, unique_id: int) -> Task:
        for task in self._tasks:
            if task.get_unique_id() == unique_id:
                return copy.deepcopy(task)
        raise Exception("get_task_from_id() task not found", unique_id)

    def get_boards_for_user(self, u: User) -> Set[Board]:
        user_boards: List[Board] = []
        for board in self._boards:
            if u in board.get_user_ids():
                user_boards.append(board)
        return set(user_boards)

    def get_user_task_assignments(self) -> Dict[int, Set[int]]:
        return copy.deepcopy(self._user_task_assignments)

    def get_user_task_assignment(self, t: Task) -> User:
        """
        get the use assigned to a certain task
        :param t: the task
        :return: the user assigned to the task
        """
        for user_id, task_ids in self._user_task_assignments.items():
            if t.get_unique_id() in task_ids:
                return self.get_user_from_id(user_id)
        raise Exception("get_user_task_assignment() task not found", t)

    def get_board_task_assignments(self) -> Dict[int, Set[int]]:
        return copy.deepcopy(self._board_task_assignments)

    def get_board_task_assignment(self, t: Task) -> Board:
        for board_id, task_ids in self._board_task_assignments.items():
            if t.get_unique_id() in task_ids:
                return self.get_board_from_id(board_id)
        raise Exception("get_board_task_assignment() task not assigned to any boards", t)

    def get_board_user_assignments(self) -> Dict[int, Set[int]]:
        return copy.deepcopy(self._board_user_assignments)

    def get_board_user_assignment(self, u: User) -> Set[Board]:
        user_boards: List[Board] = []
        for board_id, user_ids in self._board_user_assignments.items():
            if u.get_unique_id() in user_ids:
                user_boards.append(self.get_board_from_id(board_id))

        return set(user_boards)

    def get_boards(self) -> Set[Board]:
        return copy.deepcopy(self._boards)

    def get_users(self) -> Set[User]:
        return copy.deepcopy(self._users)

    def get_tasks(self) -> Set[Task]:
        return copy.deepcopy(self._tasks)

    def _get_actual_task(self, t: Task) -> Task:
        for actual_task in self._tasks:
            if t == actual_task:
                return actual_task
        raise Exception("_get_actual_task() task not found", t)

    def _get_actual_user(self, u: User) -> User:
        for actual_user in self._users:
            if u == actual_user:
                return actual_user
        raise Exception("_get_actual_user() user not found", u)

    def _get_actual_board(self, b: Board) -> Board:
        for actual_board in self._boards:
            if b == actual_board:
                return actual_board
        raise Exception("_get_actual_board() board not found", b)

    def add_board(self, b: Board):
        self._boards.add(b)
        if b.get_unique_id() not in self._board_user_assignments.keys():
            self._board_user_assignments[b.get_unique_id()] = set()
        if b.get_unique_id() not in self._board_task_assignments.keys():
            self._board_task_assignments[b.get_unique_id()] = set()

    def add_user(self, u: User):
        self._users.add(u)
        if u.get_unique_id() not in self._user_task_assignments:
            self._user_task_assignments[u.get_unique_id()] = set()

    def add_task(self, t: Task):
        self._tasks.add(t)

    def _check_task_has_user_assignment(self, t: Task) -> bool:
        for user_id, task_ids in self._user_task_assignments.items():
            if t.get_unique_id() in task_ids:
                return True
        return False

    def _check_task_has_board_assignment(self, t: Task) -> bool:
        for board_ids, task_ids in self._board_task_assignments.items():
            if t.get_unique_id() in task_ids:
                return True
        return False

    def assign_task_to_user(self, t: Task, u: User) -> bool:
        """
        assigns a task to a user
        :param t: the task to assign to the user
        :param u: the user to assign the task to
        :return: true if succeeds/false otherwise
        """
        if self._check_task_has_user_assignment(t):
            return False

        if u.get_unique_id() not in self._user_task_assignments.keys():
            return False

        self._user_task_assignments[u.get_unique_id()].add(t.get_unique_id())
        for user in self._users:
            if user == u:
                user.add_task(t)
                return True
        return False

    def assign_task_to_user_incorrect(self, t: Task, u: User) -> bool:
        """
        incorrectly assigns a task to a user
        :param t: the task to assign to the user
        :param u: the user to assign the task to
        :return: true if succeeds/false otherwise
        """
        if self._check_task_has_user_assignment(t):
            return False

        if u.get_unique_id() not in self._user_task_assignments.keys():
            return False

        for user in self._users:
            if user == u:
                user.add_task(t)
                return True
        return False

    def assign_task_to_board(self, t: Task, b: Board) -> bool:
        """
        assigns a task to a board
        :param t: the task to assign to the board
        :param b: the board to assign the task to
        :return: true if succeeds/false otherwise
        """
        if self._check_task_has_board_assignment(t):
            return False

        if b.get_unique_id() not in self._board_task_assignments.keys():
            return False

        self._board_task_assignments[b.get_unique_id()].add(t.get_unique_id())
        for board in self._boards:
            if board == b:
                board.add_task(t)
                return True
        return False

    def assign_user_to_board(self, u: User, b: Board) -> bool:
        """
        assign a user to a board
        :param u: the user to assign to the board
        :param b: the board to assign the user to
        :return: true if succeeds/false otherwise
        """
        if b.get_unique_id() not in self._board_user_assignments.keys():
            return False

        self._board_user_assignments[b.get_unique_id()].add(u.get_unique_id())
        for board in self._boards:
            if board == b:
                board.add_user(u)
                return True
        return False

    def remove_user_task_assignment(self, t: Task, u: User):
        """
        un-assign a task from a user
        :param t: the task to remove from the user
        :param u: the user to remove the task from
        """
        if self._check_task_has_user_assignment(t) \
                and t.get_unique_id() in self.get_user_from_id(u.get_unique_id()).get_task_ids():
            self._user_task_assignments[u.get_unique_id()].remove(t.get_unique_id())
            self._get_actual_user(u).remove_task(t)
        else:
            raise Exception("remove_user_task_assignment() no task assignment exists for", t, u)

    def remove_board_task_assignment(self, t: Task, b: Board):
        """
        un-assign a task from a board
        :param t: the task to remove from the board
        :param b: the board to remove the task from
        """
        if self._check_task_has_board_assignment(t) \
                and t.get_unique_id() in self.get_board_from_id(b.get_unique_id()).get_task_ids():
            self._board_task_assignments[b.get_unique_id()].remove(t.get_unique_id())
            self._get_actual_board(b).remove_task(t)
        else:
            raise Exception("remove_board_task_assignment() no board assignment exists for", t, b)

    def remove_board_user_assignment(self, u: User, b: Board):
        """
        un-assign a user from a board
        :param u: the user to remove from the board
        :param b: the board to remove the user from
        :return:
        """
        if b.get_unique_id() in self._board_user_assignments.keys() \
                and u.get_unique_id() in self.get_board_from_id(b.get_unique_id()).get_user_ids():
            self._board_user_assignments[b.get_unique_id()].remove(u.get_unique_id())
            self._get_actual_board(b).remove_user(u)
        else:
            raise Exception("remove_board_user_assignment() no board assignment exists for", u, b)

    def change_task_name(self, t: Task, new_name: str) -> Task:
        actual_task = self._get_actual_task(t)
        actual_task.set_name(new_name)
        return copy.deepcopy(actual_task)

    def change_task_desc(self, t: Task, new_desc: str) -> Task:
        actual_task = self._get_actual_task(t)
        actual_task.set_description(new_desc)
        return copy.deepcopy(actual_task)

    def complete_task(self, t: Task) -> Task:
        actual_task = self._get_actual_task(t)
        actual_task.set_completed(True)
        return copy.deepcopy(actual_task)

    def change_task_user_assignment(self, t: Task, new_user: User):
        """
        change the user assignment of a task
        :param t: the task to reassign
        :param new_user: the new user to assign the task to
        """
        if self._check_task_has_user_assignment(t):
            for user_id, task_ids in self._user_task_assignments.items():
                if t.get_unique_id() in task_ids:
                    self.remove_user_task_assignment(t, self.get_user_from_id(user_id))

        self.assign_task_to_user(t, new_user)

    def change_task_board_assignment(self, t: Task, new_board: Board):
        """
        change the board assignment of a task
        :param t: the task to reassign
        :param new_board: the new board to assign the task to
        """
        if self._check_task_has_board_assignment(t):
            for board_id, task_ids in self._board_task_assignments.items():
                if t.get_unique_id() in task_ids:
                    self.remove_board_task_assignment(t, self.get_board_from_id(board_id))

        self.assign_task_to_board(t, new_board)

    def get_tasks_from_board(self, b: Board) -> Set[Task]:
        tasks: Set[Task] = set()
        for task_id in b.get_task_ids():
            tasks.add(self.get_task_from_id(task_id))

        return tasks

    def get_users_from_board(self, b: Board) -> Set[User]:
        users: Set[User] = set()
        for user_id in b.get_user_ids():
            users.add(self.get_user_from_id(user_id))

        return users

    def get_tasks_from_user(self, u: User) -> Set[Task]:
        tasks: Set[Task] = set()
        for task_id in u.get_task_ids():
            tasks.add(self.get_task_from_id(task_id))

        return tasks

    def __str__(self):
        return f"Organization(boards: {repr(self._boards)}, " \
               f"users: {repr(self._users)}, " \
               f"tasks: {repr(self._tasks)})"

    def __repr__(self):
        return f"Organization(boards: {repr(self._boards)}, " \
               f"users: {repr(self._users)}, " \
               f"tasks: {repr(self._tasks)})"


if __name__ == "__main__":
    pass
