from itertools import count


class Task:
    __ids: count = count(0)
    _unique_id: int
    _name: str
    _description: str
    _completed: bool  # boolean value to determine if the task has been completed

    def __init__(self, name: str, description: str, unique_id: int = None):
        if unique_id is None:
            self._unique_id = next(self.__ids)
        else:
            self._unique_id = unique_id
        self._name = name
        self._description = description
        self._completed = False

    def get_unique_id(self) -> int:
        return self._unique_id

    def get_name(self) -> str:
        return self._name

    def get_description(self) -> str:
        return self._description

    def is_completed(self) -> bool:
        return self._completed

    def set_name(self, name: str):
        self._name = name

    def set_description(self, desc: str):
        self._description = desc

    def set_completed(self, completed: bool):
        self._completed = completed

    def __str__(self):
        return f"""Task {str(self._unique_id)}
  Name: {str(self._name)}
  Description: {str(self._description)}
  Completed: {str(self._completed)}"""

    def __repr__(self):
        return f"Task(unique_id: {repr(self._unique_id)}, " \
               f"name: {repr(self._name)}, " \
               f"description: {repr(self._description)}" \
               f"completed: {repr(self._completed)})"

    def __eq__(self, other):
        if other is None:
            return False

        if isinstance(other, Task):
            return self._unique_id == other._unique_id \
                   and self._name == other._name \
                   and self._description == other._description \
                   and self._completed == other._completed

        return False

    def __hash__(self):
        return hash((self._unique_id, self._name, self._description))


if __name__ == "__main__":
    pass
