from typing import Tuple, List, Set

from agile_system.organization import Organization
from agile_system.user import User
from agile_system.task import Task
from agile_system.board import Board


def print_set(items: Set):
    for e in items:
        print(str(e))


def runtime_test(org: Organization):
    john: User = User("John")
    charles: User = User("Charles")
    lily: User = User("Lily")
    piper: User = User("Piper")
    org.add_user(john)
    org.add_user(charles)
    org.add_user(lily)
    org.add_user(piper)

    parser_board: Board = Board("Parser Team")
    qe_board: Board = Board("Query Engine Team")
    org.add_board(parser_board)
    org.add_board(qe_board)

    org.assign_user_to_board(john, parser_board)
    org.assign_user_to_board(lily, parser_board)
    org.assign_user_to_board(charles, qe_board)
    org.assign_user_to_board(piper, qe_board)

    task_list: List[Tuple[Task, Board]] = [
        (Task("create database", "create a database for the warmup project"), qe_board),
        (Task("find data", "find data for the database"), qe_board),
        (Task("write interface function", "write the interface function for the query engine"), qe_board),
        (Task("create help function", "add a help function to the cli"), parser_board),
        (Task("implement regex parser for commands", "add regex parser for commands to the database"), parser_board),
        (Task("design query engine", "design the class/function structure for the query engine"), qe_board),
        (Task("implement query engine", "implement code for the query engine to query data"), qe_board),
        (Task("design parser", "design the class/function structure for the parser"), parser_board)
    ]

    for task, board in task_list:
        org.add_task(task)
        org.assign_task_to_board(task, board)

    org.assign_task_to_user(task_list[0][0], charles)
    org.assign_task_to_user(task_list[1][0], piper)
    org.assign_task_to_user(task_list[2][0], charles)
    org.assign_task_to_user(task_list[3][0], lily)
    org.assign_task_to_user(task_list[4][0], john)
    org.assign_task_to_user(task_list[7][0], john)

    org.change_task_user_assignment(task_list[0][0], piper)

    org.remove_user_task_assignment(task_list[7][0], john)

    org.complete_task(task_list[2][0])

    print("\n+================================ Tasks ================================+")
    print_set(org.get_tasks())
    print("\n+================================ Users ================================+")
    print_set(org.get_users())
    print("\n+================================ Boards ================================+")
    print_set(org.get_boards())


if __name__ == "__main__":
    org = Organization.get_instance()
    runtime_test(org)
