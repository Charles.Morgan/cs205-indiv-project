# Individual Project Report
###### Charles Morgan

For this project I decided to model the data classes for a simple agile software system
such as trello. My implementation of this system includes tasks, users, boards, and organization
classes. The organization class follows a singleton pattern. There are unit tests for all complex
functions in this system.

## Diagram of classes

```
Task
  variables
    __ids: itertools.count
    _unique_id: int
    _name: str
    _description: str
  methods
    __init__(str, str, Optional[int]): Task
    get_unique_id(): int
    get_name(): str
    get_description(): str
    is_completed(): bool
    set_name(str): None
    set_description(str): None
    set_completed(bool): None
    __str__(): str
    __repr__(): str
    __eq__(): bool
    __hash__(): int

User
  variables
    __ids: itertools.count
    _unique_id: int
    _name: str
    _task_ids: Set[int]
  methods
    __init__(str, Optional[int], Optional[Set[int]]): User
    get_unique_id: int
    get_name: str
    get_task_ids: Set[int]
    set_name(str): None
    clear_tasks(): None
    add_task(Task): None
    remove_task(Task): None
    __str__(): str
    __repr__(): str
    __eq__(): bool
    __hash__(): int

Board
  variables
    __ids: itertools.count
    _unique_id: int
    _name: str
    _task_ids: Set[int]
    _user_ids: Set[int]
  methods
    __init__(str, Optional[int], Optional[Set[int]], Optional[Set[int]]): Board
    get_unique_id(): int
    get_name(): str
    get_user_ids(): Set[int]
    get_task_ids(): Set[int]
    set_name(str): None
    add_user(User): None
    remove_user(User): None
    add_task(Task): None
    remove_task(Task): None
    __str__(): str
    __repr__(): str
    __eq__(): bool
    __hash__(): int

Organization
  variables
    __instance: Organization
    _boards: Set[Board]
    _users: Set[User]
    _tasks: Set[Task]
    _user_task_assignments: Dict[int, Set[int]]
    _board_task_assignments: Dict[int, Set[int]]
    _board_user_assignments: Dict[int, Set[int]]
  methods
    get_instance(): Organization
    __init__(): Organization
    clear_organization(): None
    get_board_from_id(int): Board
    get_user_from_id(int): User
    get_task_from_id(int): Task
    get_boards_for_user(User): Set[Board]
    get_user_task_assignments(): Dict[int, Set[int]]
    get_user_task_assignment(Task): User
    get_board_task_assignments(): Dict[int, Set[int]]
    get_board_task_assignment(Task): Board
    get_board_user_assignments(): Dict[int, Set[int]]
    get_board_user_assignment(User): Board
    get_boards(): Set[Board]
    get_users(): Set[User]
    get_tasks(): Set[Task]
    _get_actual_task(Task): Task
    _get_actual_user(User): User
    _get_actual_board(Board): Board
    add_board(Board): None
    add_user(User): None
    add_task(Task): None
    _check_task_has_user_assignment(Task): bool
    _check_task_has_board_assignment(Task): bool
    assign_task_to_user(Task, User): bool
    assign_task_to_user_incorrect(Task, User): bool
    assign_task_to_board(Task, Board): bool
    assign_user_to_board(User, Board): bool
    remove_user_task_assignment(Task, User): None
    remove_board_task_assignment(Task, Board): None
    remove_board_user_assignment(User, Board): None
    change_task_name(Task, str): Task
    change_task_desc(Task, str): Task
    complete_task(Task): Task
    change_task_user_assignment(Task, User): None
    change_task_board_assignment(Task, Board): None
    get_tasks_from_board(Board): Set[Task]
    get_users_from_board(Board): Set[User]
    get_tasks_from_user(User): Set[Task]
    __str__(): str
    __repr__(): str
```

## Description of Tests

##### Task Tests

- *test_task_initialization* is self explanatory
- *test_task_ids* tests that the unique id functionality works as intended. Each task should have
  a different unique id and this is done by keeping a counter and adding one every time an instance
  of the Task class is created. This test succeeds if the actual unique id values are the same as the
  expected values.
- *test_task_getters* is self explanatory
- *test_task_setters* is self explanatory
- *test_task_equals* is self explanatory

##### User Tests

- *test_user_initialization* is self explanatory
- *test_user_ids* tests that the unique id functionality works as intended. Each user should have
  a different unique id and this is done by keeping a counter and adding one every time an instance
  of the User class is created. This test succeeds if the actual unique id values are the same as the
  expected values.
- *test_user_getters* is self explanatory
- *test_user_setters* is self explanatory
- *test_user_add_task* tests adding a task to the task id set. This test passes if the method
  successfully adds a task to the task id set
- *test_user_remove_task* tests removing a task from the task id set. This test passes if the method
  successfully removes a task from the task id set
- *test_user_equality* is self explanatory
  
##### Board Tests

- *test_board_initialization* is self explanatory
- *test_board_getters* is self explanatory
- *test_board_setters* is self explanatory
- *test_board_add_user* tests adding a user to the user id set. This test passes if the method
  successfully adds a user to the user id set
- *test_board_remove_user* tests removing a user from the user id set. This test passes if the method
  successfully removes a user from the user id set
- *test_board_add_task* tests adding a task to the task id set. This test passes if the method
  successfully adds a task to the task id set
- *test_board_remove_task* tests removing a task from the task id set. This test passes if the method
  successfully removes a task from the task id set
- *test_board_equality* is self explanatory

##### Organization Tests

- *test_assign_task_to_user* tests assigning a task to a user. This test passes if the task has been
  added to the users task id set and if the user task assignment has been created
- *test_assign_task_to_user_incorrect* tests incorrectly assigning a task to a user. This test will
  not pass because the function doesn't create a user task assignment
- *test_remove_task_from_user* tests un-assigning a user to a task. This test passes if the task
  gets removed from the users task id set and if the user task assignment gets deleted
- *test_change_task_user_assignment* tests changing the assignee of a task. This test passes if the task
  is successfully removed from the original users task list, added to the new users task list and the 
  user task assignment has been correctly updated to the new user
- *test_assign_task_to_board* tests assigning a task to a board. This test passes if the task has been
  added to the boards task id set and if the board task assignment has been created
- *test_remove_task_from_board* tests un-assigning a task to a board. This test passes if the task
  gets removed from the boards task id set and if the board task assignment gets deleted
- *test_change_task_board_assignment* tests changing the assigned board of a task. This test passes if the task
  is successfully removed from the original boards task list, added to the new boards task list and the
  board task assignment has been correctly updated to the new board
- *test_assign_user_to_board* tests assigning a user to a board. This test passes if the user has been
  added to the boards user id set and if the board user assignment has been created
- *test_remove_user_from_board* tests un-assigning a user to a board. This test passes if the user
  gets removed from the boards user id list and if the board user assignment gets deleted
